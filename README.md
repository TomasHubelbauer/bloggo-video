# Video

> Free (beer or money) open source stuff concerning online video

- Check out [Shortcut](https://www.shotcutapp.com/)
- Check out [AV1](https://en.wikipedia.org/wiki/AV1)
    - [This announcement](https://aomedia.org/the-alliance-for-open-media-kickstarts-video-innovation-era-with-av1-release/)
    - [Blog post](https://people.xiph.org/~xiphmont/demo/av1/demo1.shtml)

## OBS  (Open Broadcaster Software)

[Web site](https://obsproject.com/)

## The History of Video Synthesizers

https://wearethemutants.com/2018/01/09/a-sloppy-machine-like-me-the-history-of-video-synthesizers/
